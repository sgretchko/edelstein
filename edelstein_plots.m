format long

maxit = 250;
values = zeros(1,maxit);
for n = 1:maxit
    % calculate norm of R^n(0) squared
    sum = 0;
    for k = 1:n
        arg = pi*n/factorial(k);
        ksi_k = 1;
        term = 4*sin(arg)^2*ksi_k^2;
        sum = sum + term;
    end
    values(n) = sum;
end

% Plot
plot(values, 'LineWidth', 0.75)
% Add title and axis labels
label = ['Classical Edelstein after ',num2str(maxit),' iterations'];
%title(label)
xlabel('Iteration $n$', 'Interpreter', 'latex')
ylabel('$|\!| \textbf{R}^n 0 |\!|^2$', 'Interpreter', 'latex')
grid on
name = ['s',num2str(maxit)];
name_png = [name,'.png'];
name_eps = [name,'.eps'];
print(gcf, name_png,'-dpng','-r600')
print(gcf, name_eps,'-depsc')
