import numpy as np
from math import factorial, sin
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure

# This code was converted from the Matlab code contained in edelstein_plots.m

maxit = 250
values = np.zeros(maxit)
for n in range(1, maxit+1):
    # calculate norm of R^n(0) squared
    sum = 0
    for k in range(1, n+1):
        try:
            fact_k = float(factorial(k))
        except OverflowError:
            break # no need to go further as arg will be close to zero
                  # (Matlab gives inf when this happens)
        arg = np.pi*n/fact_k
        ksi_k = 1
        term = 4 * sin(arg)**2 * ksi_k**2
        sum += term
    values[n-1] = sum

xLabel = 'Iteration $n$'
yLabel = r'$|\!| \mathbf{R}^n 0 |\!|^2$'
xRange = range(1, len(values)+1)
f = plt.figure(figsize = (9, 7))
plt.plot(xRange,values)
plt.xlabel(xLabel)
plt.ylabel(yLabel)
plt.grid(True)
plt.show()


