# Python 3 code

def fact(n):
    f = 1
    for i in range (1, int(n)+1):
        f = f * i
    return f

def numDigits(n):
    n_str = str(n) # convert integer n into a string
    digitCount = len(n_str)
    if n < 0:
        digitCount -= 1 # take into account sign
    return digitCount 

n_max = 6
# print the first n_max terms of the sequence
for n in range(1, n_max+1):
    e_n = 0
    for m in range(1, n+1):
        e_n += fact(n * 2**m)
    e_n //= 2  # do integer division to avoid floating point numbers
    digitCount = numDigits(e_n)

    result = "e" + str(n) + ": "
    if n <= 3:
        result += str(e_n)
    else:
        result += str(digitCount) + " digits"
    print(result)

