This repository contains the code used in the paper "Edelstein’s astonishing afﬁne isometry".
The Matlab script eldestein_plots.m evaluates and plots as a function of *n* the expression

![Alt text](images/edelstein_formula.png?raw=true "Formula"){width=107 height=34}

This is the script that was used to generate the plots in the paper. This code was also 
converted to Python and can be found in the file eldelstein_plots.py.

The file edelstein_suborbit.py contains the Python code that computed the sequence of 
indices (*e_n*) described in section 6 of the paper.